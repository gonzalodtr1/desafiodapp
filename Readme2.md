# Ampliación de Dapp

Con motivo de una mejora de la seguridad de nuestra Dapp, se han implementado algunos cambios en el contrato que gestiona dicha Dapp.

En este pequeño anexo comentaré los añadidos que se han hecho dentro del contrato.

En primer lugar, hay que destacar que para implementar estas mejoras he hecho uso de la librería de OpenZeppelin que es una de las más famosas
en lo que se refiere a seguridad en SmartContracts.

Para ello el primer paso debe ser instalarla:

![Imagen1](Capturas/installation_openzepellin.png)

Una vez la tenemos instalada podemos trabajar con ella.

A continuación paso a explicar las partes nuevas del contrato.

En el inicio del contrato es necesario importar las partes de la librería que vamos a utilizar.
En nuestro caso utilizaremos los siguiente módulos:
-*Ownable*: este módulo se utiliza por herencia. Pone a disposición el modificador `onlyOwner`, el cual puede ser aplicado a sus funciones para restringir su uso únicamente al propietario.
-*Pausable*: este módulo también se utiliza por herencia. Pone a disposición los modificadores `whenNotPaused` y `whenPaused`, que pueden ser aplicados en las funciones de tu contrato. 
-*ECDSA*: es usado en el contrato para recuperar la dirección del firmante del mensaje y hacer validaciones antes de permitir la votación.
```
import "@openzeppelin/contracts/ownership/Ownable.sol";
import "@openzeppelin/contracts/lifecycle/Pausable.sol";
import "@openzeppelin/contracts/cryptography/ECDSA.sol";


// Definicion del nombre del contrato
contract Challenge is Ownable, Pausable{
    using ECDSA
    for bytes32;
```

Además de importar dichos módulos definimos las propiedades que el contrato podrá usar de OpenZeppelin a partir de los módulos importados.


Por último, observamos la implementación en sí. 
Encontramos primero la definición de un flag que utilizaremos para detener el contracto si fuera necesario.

```
    bool private stopped = false;

```

Luego encontramos una función que será encargada de cambiar el flag cuando por algún error sea necesario la detención del código que se encuentra corriendo.
Esto es lo que se llama un *circuit-breaker*. Como observamos utiliza el modificador *onlyOwner* que comentamos anteriormente.

También encontramos dos modificadores de los que podremos hacer uso. Uno de ellos para detener la ejecución de una función en caso de emergencia, y 
el otro para ejecutar una función en caso de emergencia.

```

function toggleContractActive() onlyOwner public {
        stopped = !stopped;
    }
  
modifier stopInEmergency {
        if (!stopped) _;
    }
    
modifier onlyInEmergency {
        if (stopped) _;
    }
    
```

Por último nos vamos a la función *vote* y allí es donde encontramos el modificador *stopInEmergency*, que con lo que acabamos de observar,
solo podrá ser ejecutado por el dueño del contrato en caso de emergencia.

```

 function vote (uint _challengerId) stopInEmergency public {

...

```

Para terminar lanzamos el *migrate* del contrato para observar que no hay ningún problema en compilación:

![Imagen2](Capturas/migrate_withOZ.png)

Observamos que todo continua sin ningún problema.

Con esto hemos asegurado algo más nuestro contrato en caso de posibles errores.
