# DesafioDapp

En este ejercicio se describirá la implementación de una aplicación descentralizada cuya finalidad es la **gestión de una
votación entre dos clubes de fútbol**.

Se procederá a la construcción de una aplicación gestionada descentralizadamente en la que podrá votar cualquier persona con una
cuenta de Ethereum.

Antes de comenzar con la explicación es necesario destacar que para desplegar y utilizar esta dApp es necesario tener instaladas algunas herramientas
como pueden ser NPM, Truffle, Ganache, MetaMask o IPFS

## Smart Contract
Para comenzar a desarrollar el smart contract que será la base de la dApp antes se ha tomado como punto de partida la conocida
dApp de la tienda de mascotas Pet Shop Box ([](https://github.com/truffle-box/pet-shop-box))
Una vez se desempaquetado la dApp de la tienda de animales hemos comenzado desde aquí el desarrollo del contrato.
Se accede a la carpeta de *contracts* y hemos creado el fichero *Challenge.sol* que será el que albergará el contrato de nuestra dApp.
El código del contrato a medida ue se desarrollaba se iba probando en la herramienta online [](https://remix.ethereum.org)
Aunque el contracto se encuentra comentado en el código haremos un breve análisis de lo que contiene.

En primer lugar, se declara la versión de Solidity para la que está implementado, se define el nombre del contrato y
se define la estructura *Challenger* con sus atributos que será utilizada por cada uno de los aspirantes de la votación.
Además se definen los mapeos para guardar a los aspirantes y las cuentas. Al final también vemos que se define el número de aspirantes.

```
pragma solidity ^0.5.0;

contract Challenge {
    struct Challenger {
        uint id;
        string name;
        uint voteCount;
    }

    mapping(uint => Challenger) public challengers;

    mapping(address => bool) public voters;

    uint public challengersCount;
```

A continuación se declara el evento a través del cual se votará y se crea el constructor, dónde se inicializa el contrato
y se definen a los dos aspirantes de la votación.
```
    event votedEvent (
        uint indexed _challengerId
    );

    constructor () public {
        addChallenger("Real Madrid CF");
        addChallenger("FC Barcelona");
    }
```

Lo siguiente que encontramos es la función para añadir aspirantes a partir del mapeo que definimos anteriormente.
```
    function addChallenger (string memory _name) private {
        challengersCount ++;
        challengers[challengersCount] = Challenger(challengersCount, _name, 0);
    }
```
Por último encontramos la función de votación del aspirante que deseemos. Se realizan dos comprobaciones para evitar fraudes
y a continuación se computa y emite el voto.
```
    function vote (uint _challengerId) public {
        require(!voters[msg.sender]);
        require(_challengerId > 0 && _challengerId <= challengersCount);
        voters[msg.sender] = true;
        challengers[_challengerId].voteCount ++;
        emit votedEvent(_challengerId);
    }
```

Para más explicación acceder al archivo *Challenge.sol* alojado en la carpeta *contracts*.

##Tests Smart Contract

Una vez que tenemos el contrato terminado vamos a realizar una serie de test con el objetivo de comprobar que el contrato funciona como esperamos.
Para ello dentro de la carpeta *test* hemos creado un archivo *challenge.js* donde declararemos los tests.
Al igual que en el contrato haremos un breve análisis de los test implementados.

En primer lugar se trae el contrato que se va a probar y se le asigna a una variable y se llama a la funcion *contract*,
dentro de la función callback es donde se desarrollarán todos los tests.
```
var Challenge = artifacts.require("./Challenge.sol");

contract("Challenge", function(accounts) {
  var challengeInstance;
```

En el primer test se comprueba que un votante con su cuenta es capaz de votar.
Se despliega una instancia del contrato y a partir de ahí se van haciendo comprobaciones para verificar que se ha emitido y registrado el voto.
```
  it("Es posible realizar un voto por un votante", function() {
      return Challenge.deployed().then(function(instance) {
        challengeInstance = instance;
        challengerId = 1;
        return challengeInstance.vote(challengerId, { from: accounts[0] });
      }).then(function(receipt) {
        assert.equal(receipt.logs.length, 1, "Se lanza un evento");
        assert.equal(receipt.logs[0].event, "votedEvent", "El tipo de evento es correcto");
        assert.equal(receipt.logs[0].args._challengerId.toNumber(), challengerId, "El challenger id es correcto");
        return challengeInstance.voters(accounts[0]);
      }).then(function(voted) {
        assert(voted, "El votante ha sido registrado como que ha votado");
        return challengeInstance.challengers(challengerId);
      }).then(function(challenger) {
        var voteCount = challenger[2];
        assert.equal(voteCount, 1, "Se incrementa el número de votos a uno de los aspirantes");
      })
    });
```

Este segundo test comprueba que cuando hay un aspirante incorrecto no hay votación y se lanza una excepción.
Se despliega una instancia del contrato y a partir de ahí se van haciendo comprobaciones para verificar que no se ha emitido el voto y que se ha lanzado un error.
```
    it("Lanzamiento de una excepción cuando hay un aspirante incorrecto", function() {
      return Challenge.deployed().then(function(instance) {
          challengeInstance = instance;
        return challengeInstance.vote(99, { from: accounts[1] })
      }).then(assert.fail).catch(function(error) {
        assert(error.message.indexOf('revert') >= 0, "El mensaje de error debe contener un 'revert'");
        return challengeInstance.challengers(1);
      }).then(function(challenger1) {
        var voteCount = challenger1[2];
        assert.equal(voteCount, 1, "Real Madrid no recibió ningún voto");
        return challengeInstance.challengers(2);
      }).then(function(challenger2) {
        var voteCount = challenger2[2];
        assert.equal(voteCount, 0, "Barcelona no recibió ningún voto");
      });
    });
```

El tercer y último test comprueba que no es posible realizar una votacíon doble desde la misma cuenta.
Se despliega una instancia del contrato y a partir de ahí se van haciendo comprobaciones para verificar que
se ha emitido y registrado el primer voto y no se ha emitido el segundo voto y que se ha lanzado un error.
```
    it("Lanzamiento de una excepción por votación doble", function() {
      return Challenge.deployed().then(function(instance) {
          challengeInstance = instance;
          challengerId = 2;
        challengeInstance.vote(challengerId, { from: accounts[1] });
        return challengeInstance.challengers(challengerId);
      }).then(function(challenger) {
        var voteCount = challenger[2];
        assert.equal(voteCount, 1, "Se acepta el primer voto");
        return challengeInstance.vote(challengerId, { from: accounts[1] });
      }).then(assert.fail).catch(function(error) {
        assert(error.message.indexOf('revert') >= 0, "El mensaje de error debe contener un 'revert'");
        return challengeInstance.challengers(1);
      }).then(function(challenger1) {
        var voteCount = challenger1[2];
        assert.equal(voteCount, 1, "Real Madrid no recibió ningún voto");
        return challengeInstance.challengers(2);
      }).then(function(challenger2) {
        var voteCount = challenger2[2];
        assert.equal(voteCount, 1, "Barcelona no recibió ningún voto");
      });
    });
```

## Interfaz de Usuario
Una vez que el contrato se puede compilar y desplegar y los test que hemos implementado para el contrato pasan correctamente
(ambas cosas se demostrarán en el siguiente apartado), se ha procedido al desarrollo de la interfaz de usuario.

El desarrollo de la interfaz de usuario consta de dos partes, por un lado el fichero HTML *index.html* que se encuentra en la
carpeta *src* y por otro lado el JavaScript que gestiona este HTML *app.js* que se encuentra en la carpeta *src>js*.

Empezaremos analizando un poco el *index.html*.

En primer lugar, se declara el idioma, el formato de codificación y el título además de las dependencias que utilizaremos.
´´´
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>El Desafío</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/web3.min.js"></script>
    <script src="js/truffle-contract.js"></script>
    <script src="js/app.js"></script>

    <link href="css/bootstrap.min.css" rel="stylesheet">
´´´
A continuación observamos dos partes bien diferenciadas. 
Una primera donde se define la cabecera de la web y se dibuja el formulario del recuento de votos con la cuenta ligada
al contrato debajo de dicho formulario.
```
  </head>
  <body>
    <div class="container" style="width: 650px;">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="text-center">El Desafío</h1>
          <img id="rmadrid" src="./images/rmadrid.png" style="height:80px; width: 80px; float:left">
          <img id="barcelona" src="./images/barcelona.png" style="height:70px; width: 70px; float:right">
          <h4 class="text-center">¿Y tú, de quién eres?</h4>
          <hr/>
          <br/>
          <div id="loader">
            <p class="text-center">Cargando...</p>
          </div>
          <div id="content" style="display: none;">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Votos</th>
                </tr>
              </thead>
              <tbody id="challengersResults">
              </tbody>
            </table>
            <hr/>
            <p id="accountAddress" class="text-center"></p>
          </div>
        </div>
      </div>
```

La segunda parte donde tenemos un desplegable con los posibles aspirantes a ser votados y un botón que desencadenará la votación.
```
      <form onSubmit="App.castVote(); return false;">
        <div class="form-group">
          <label for="challengersSelect">Elige uno de los aspirantes</label>
          <select class="form-control" id="challengersSelect">
          </select>
        </div>
        <button type="submit" class="btn btn-primary">Votar</button>
        <hr />
      </form>      
    </div>
  </body>
</html>
```

Por otro lado vamos a analizar el fichero *app.js*:


En primer lugar, como se hace en muchas dApps se define y se inicializa la aplicación y a continuación se llama a initWeb3. 
```
App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',

  init: function() {
    return App.initWeb3();
  },
```

A continuación encontramos la función initWeb3 que inicializa una instancia de web3 para comunicarse con el contrato.
Observamos como se comprueba si hay una instancia ya creada y si no la hay se crea una en la blockchain local que lanzaremos con Ganache.
```
  initWeb3: function() {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      ethereum.enable();
      web3 = new Web3(web3.currentProvider);
    } else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      ethereum.enable();
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },
```

A continuación encontramos dos funciones, la función *initcontract* realiza la inicialización del contrato, abriendo la comunicación via JSON.
Aquí se conecta el contrato con el proveedor de web3 y e pone a escuchar si existe algún evento y si ocurriese lo pasa al front con las medidas necesarias.
Por otro lado tenemos la función que se encuentra esperando por la llegada de algún evento para recargar el front.
```
  initContract: function() {
    $.getJSON("Challenge.json", function(challenge) {
      // Instanciación de un nuevo contrato truffle
      App.contracts.Challenge = TruffleContract(challenge);
      // Conexión al proveedor para interactuar con el contrato
      App.contracts.Challenge.setProvider(App.web3Provider);

      App.listenForEvents();

      return App.writeFront();
    });
  },

  listenForEvents: function() {
    App.contracts.Challenge.deployed().then(function(instance) {
      instance.votedEvent({}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event)
        // Recarga de la página cuando un nuevo voto sea registrado
        App.writeFront();
      });
    });
  },
```

Si seguimos analizando el código lo siguiente que nos encontramos es la parte donde se escribirá en el front los datos de los aspirantes y de la cuenta una vez
se haya cargado el contrato y los datos de la cuenta.
Además aquí se actualizarán los votos en el front y se vetará el doble votado a través del frontend.
```
writeFront: function() {
    var challengeInstance;
    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    // Carga de los datos de la cuenta
    web3.eth.getAccounts(function(err, accounts) {
      if (err === null) {
        App.account = accounts[0];
        $("#accountAddress").html("Your Account: " + accounts[0]);
      }
    });

    // Carga de los datos del contrato
    App.contracts.Challenge.deployed().then(function(instance) {
      challengeInstance = instance;
      return challengeInstance.challengersCount();
    }).then(function(challengersCount) {
      var challengersResults = $("#challengersResults");
      challengersResults.empty();

      var challengersSelect = $('#challengersSelect');
      challengersSelect.empty();

      for (var i = 1; i <= challengersCount; i++) {
        challengeInstance.challengers(i).then(function(challenger) {
          var id = challenger[0];
          var name = challenger[1];
          var voteCount = challenger[2];

          // Escritura de los datos de los aspirantes
          var challengerTemplate = "<tr><th>" + id  + "</th><td>" + name + "</td><td>" + voteCount + "</td></tr>";
          challengersResults.append(challengerTemplate);

          // Escritura de los nombres de los candidatos en el desplegable
          var challengerOption = "<option value='" + id + "' >" + name + "</ option>";
          challengersSelect.append(challengerOption);
        });
      }
      return challengeInstance.voters(App.account);
  }).then(function(hasVoted) {
    // Cerrar la opción de votar cuando ya se haya votado
    if(hasVoted) {
      $('form').hide();
    }
      loader.hide();
      content.show();
    }).catch(function(error) {
      console.warn(error);
    });
  },
```

La última función implementada que nos encontramos sirve para la gestión de los votos dentro de la instacia del contrato levantada.
```
castVote: function() {
    var challengerId = $('#challengersSelect').val();
    App.contracts.Challenge.deployed().then(function(instance) {
      return instance.vote(challengerId, { from: App.account });
    }).then(function(result) {
      // Esperar al voto hasta actualizar
      $("#content").hide();
      $("#loader").show();
    }).catch(function(err) {
      console.error(err);
    });
  }
};
```
Para terminar encontramos la función que inicia el programa cuando abrimos la ventana.
```
$(function() {
  $(window).load(function() {
    App.init();
  });
});

```

## Despliegue y ejecución de la DApp

En este apartado se explicará paso a paso que hacer para lanzar la aplicación.
En primer lugar es necesario abrir una blockchain local, esto será sencillo puesto que abriendo Ganache ya estaría solucionado.

![Imagen1](Capturas/ganache.png)

Una vez tenemos la blockchain lanzada se realizará un *migrate* del contrato desde la raiz del directorio del proyecto.

![Imagen2](Capturas/migrate.png)

Como observamos que todo ha ido correctamente, es el momento para lanzar los test que hemos desarrollado.

![Imagen3](Capturas/test.png)

Comprobamos que todos los test pasan correctamente, por tanto ya podemos lanzar la aplicación al completo. Para ello es necesario que tengamos MetaMask conectado en 
la red local y en el mismo puerto que la blockchain.

![Imagen4](Capturas/ejecucion.png)

Al momento de haber lanzado la aplicación se nos abre una pestaña de nuestro navegador con la aplicación y la petición de MetaMask de conectarse con la aplicación.

![Imagen5](Capturas/dAppFront.PNG)

Conectamos nuestra cuenta con la aplicación utilizando MetaMask y se nos rellenan los datos del formulario y el desplegable. Ahora podremos disponernos a votar.
Clickamos en el botón y MetaMask nos pedirá confirmación de la transacción.

![Imagen6](Capturas/dAppFront2.PNG)

Una vez realizada la transacción que únicamente consistirá en gas que se ha gastado, podremos observarla en Ganache.

![Imagen7](Capturas/ganache_transaction.png)

Por último si volvemos a la aplicación observamos cómo se han actualizado los votos y que ya no tenemos permiso para volver a votar.

![Imagen8](Capturas/dAppFront3.PNG)

## Despliegue de contrato en Rinkeby y de la aplicación en IPFS

Para terminar hemos decidido subir el contrato a la testnet de Rinkeby. Para realizar esto se configura en primer lugar el *truffle-config.js* para que acepte la red Rinkeby.
A continuación, es necesario tener una cuenta con fondos en la red y deshabilitar la contraseña de esta

![Imagen9](Capturas/rinkeby_unablePass.png)

Una vez hecho esto se migra el contrato a dicha red.

![Imagen10](Capturas/migrate_rinkeby1.png)
![Imagen11](Capturas/migrate_rinkeby1.png)
![Imagen12](Capturas/migrate_rinkeby1.png)
![Imagen13](Capturas/Selección_004.png)

Y con esto ya tendríamos el contrato alojado en la red de Rinkeby y podríamos interactuar con él.

Por otro lado, vamos a subir nuestra aplicación al sistema de archivos IPFS.
Para ello, inicializamos ipfs y lanzamos el demonio de IPFS

![Imagen14](Capturas/ipfs_daemon.png)

A continuación solo será necesario subir los archivos de la carpeta /src (es necesario acordarse de introducir en dicha carpeta los contratos).

![Imagen15](Capturas/ipfs_up.png)

Y con esto, nuestra aplicación ya estaría subida a ipfs.

![Imagen15](Capturas/dAppFront_ipfs.png)

![Imagen16](Capturas/dAppFront_ipfs2.png)

Y como observamos si realizamos una votación se registra en la red de Rinkeby dicha transacción.

![Imagen17](Capturas/etherscan_rinkeby.png)

Y esta sería la sencilla aplicación para la votación de El Desafío y su ejecución.
