// En primer lugar se trae el contrato que se va a probar y se le asigna a una variable
var Challenge = artifacts.require("./Challenge.sol");

/*
Se llama a la función 'contract',  y dentro de la función de callback
escribimos todos los tests ya que esta nos da una variable 'accounts'
que representa todas las cuentas de nuestra blockchain
*/
contract("Challenge", function(accounts) {
  var challengeInstance;

  //Este primer test comprueba que cada votante puede realizar un voto
  it("Es posible realizar un voto por un votante", function() {
      // Se despliega una instancia del contracto en primer lugar
      return Challenge.deployed().then(function(instance) {
        challengeInstance = instance;
        challengerId = 1;
        // Se utiliza la función para votar con el Id=1 desde una de las cuentas
        return challengeInstance.vote(challengerId, { from: accounts[0] });
      }).then(function(receipt) {
        //Se comprueba que se ha lanzado el evento de votación y con el Id=1
        assert.equal(receipt.logs.length, 1, "Se lanza un evento");
        assert.equal(receipt.logs[0].event, "votedEvent", "El tipo de evento es correcto");
        assert.equal(receipt.logs[0].args._challengerId.toNumber(), challengerId, "El challenger id es correcto");
        return challengeInstance.voters(accounts[0]);
      }).then(function(voted) {
        // Se observa que el votante se encuentra registrado
        assert(voted, "El votante ha sido registrado como que ha votado");
        return challengeInstance.challengers(challengerId);
      }).then(function(challenger) {
        var voteCount = challenger[2];
        // Se observa que el aspirante votado tiene un voto
        assert.equal(voteCount, 1, "Se incrementa el número de votos a uno de los aspirantes");
      })
    });

    //Este segundo test comprueba que cuando hay un aspirante incorrecto se lanza una excepción
    it("Lanzamiento de una excepción cuando hay un aspirante incorrecto", function() {
      // Se despliega una instancia del contracto en primer lugar
      return Challenge.deployed().then(function(instance) {
          challengeInstance = instance;
        // Se utiliza la función para votar con el Id=99 desde una de las cuentas
        return challengeInstance.vote(99, { from: accounts[1] })
      }).then(assert.fail).catch(function(error) {
        //Se comprueba que se ha lanzado una mensaje de error con un revert
        assert(error.message.indexOf('revert') >= 0, "El mensaje de error debe contener un 'revert'");
        return challengeInstance.challengers(1);
      }).then(function(challenger1) {
        var voteCount = challenger1[2];
        //Se comprueba que el primer aspirante no ha recibido nuevos votos
        assert.equal(voteCount, 1, "Real Madrid no recibió ningún voto");
        return challengeInstance.challengers(2);
      }).then(function(challenger2) {
        var voteCount = challenger2[2];
        //Se comprueba que el segundo aspirante no ha recibido nuevos votos
        assert.equal(voteCount, 0, "Barcelona no recibió ningún voto");
      });
    });

    //Este tercer test comprueba que cuando una cuenta vota dos veces se lanza una excepción
    it("Lanzamiento de una excepción por votación doble", function() {
      // Se despliega una instancia del contracto en primer lugar
      return Challenge.deployed().then(function(instance) {
          challengeInstance = instance;
          challengerId = 2;
        // Se utiliza la función para votar con el Id=2 desde una de las cuentas
        challengeInstance.vote(challengerId, { from: accounts[1] });
        return challengeInstance.challengers(challengerId);
      }).then(function(challenger) {
        var voteCount = challenger[2];
        //Se comprueba que se ha lanzado el evento de votación y que el conteo ha aumentado
        assert.equal(voteCount, 1, "Se acepta el primer voto");
        // Se intenta volver a votar con la misma cuenta
        return challengeInstance.vote(challengerId, { from: accounts[1] });
      }).then(assert.fail).catch(function(error) {
        //Se comprueba que se ha lanzado una mensaje de error con un revert
        assert(error.message.indexOf('revert') >= 0, "El mensaje de error debe contener un 'revert'");
        return challengeInstance.challengers(1);
      }).then(function(challenger1) {
        var voteCount = challenger1[2];
        //Se comprueba que el primer aspirante no ha recibido nuevos votos
        assert.equal(voteCount, 1, "Real Madrid no recibió ningún voto");
        return challengeInstance.challengers(2);
      }).then(function(challenger2) {
        var voteCount = challenger2[2];
        //Se comprueba que el segundo aspirante no ha recibido nuevos votos
        assert.equal(voteCount, 1, "Barcelona no recibió ningún voto");
      });
    });
  });