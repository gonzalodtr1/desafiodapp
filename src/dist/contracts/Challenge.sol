pragma solidity ^0.5.0;


// Definicion del nombre del contrato
contract Challenge {
    // Creación de la estructura para los aspirantes con sus atributos
    struct Challenger {
        uint id;
        string name;
        uint voteCount;
    }

    // Mapeo para guardar a los competidores
    mapping(uint => Challenger) public challengers;

    // Mapeo para guardar las cuentas de quienes hayan votado
    mapping(address => bool) public voters;

    // Definición del número de aspirantes
    uint public challengersCount;

    // Declaración de el evento de votación
    event votedEvent (
        uint indexed _challengerId
    );

    //Se crea un constructor para inicializar el contrato donde se definen dos competidores
    constructor () public {
        addChallenger("Real Madrid CF");
        addChallenger("FC Barcelona");
    }

    /* 
    Función para añadir aspirantes a partir del mapeo creado
    La función toma un argumento de tipo string (el nombre del aspirante), suma uno al contador (esto significa que hay un nuevo competidor)
    y entonces el mapeo se actualiza con la inicialización de un nuevo aspirante (con todos sus atributos)
    */
    function addChallenger (string memory _name) private {
        challengersCount ++;
        challengers[challengersCount] = Challenger(challengersCount, _name, 0);
    }

    /*
    Esta función tiene un argumento que el ID del aspirante.
    Tiene visibilidad pública porque una cuenta externa tiene que ser capaz de llamarla.
    La función tiene dos 'requires', el primero para comprobar que el votante no haya votado antes
    y el segundo para comprobar que el competidor elegido es válido.
    La función finalmente guarda el nuevo votante y aumenta el número de votos hechos a uno de los aspirantes.
    Finalmente se lanza el evento cuando el voto haya sido hecho.
    */
    function vote (uint _challengerId) public {
        // Exigencia de que el votante no haya votado antes
        require(!voters[msg.sender]);

        // Exigencia de que el aspirante sea válido
        require(_challengerId > 0 && _challengerId <= challengersCount);

        // Guardado de un nuevo votante
        voters[msg.sender] = true;

        // Actualización del conteo de los votos
        challengers[_challengerId].voteCount ++;

        // Lanzamiento del evento de votación
        emit votedEvent(_challengerId);
    }

}