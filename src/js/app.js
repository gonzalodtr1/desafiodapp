App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    if (typeof web3 !== 'undefined') {
      // Si una instancia de web3 ya se ha proporcionado pro MetaMask
      App.web3Provider = web3.currentProvider;
      ethereum.enable();
      web3 = new Web3(web3.currentProvider);
    } else {
      //Instancia por defecto si no hay una instancia web3
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      ethereum.enable();
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },

  initContract: function() {
    $.getJSON("Challenge.json", function(challenge) {
      // Instanciación de un nuevo contrato truffle
      App.contracts.Challenge = TruffleContract(challenge);
      // Conexión al proveedor para interactuar con el contrato
      App.contracts.Challenge.setProvider(App.web3Provider);

      App.listenForEvents();

      return App.writeFront();
    });
  },

  listenForEvents: function() {
    App.contracts.Challenge.deployed().then(function(instance) {
      instance.votedEvent({}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event)
        // Recarga de la página cuando un nuevo voto sea registrado
        App.writeFront();
      });
    });
  },

  writeFront: function() {
    var challengeInstance;
    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    // Carga de los datos de la cuenta
    web3.eth.getAccounts(function(err, accounts) {
      if (err === null) {
        App.account = accounts[0];
        $("#accountAddress").html("Your Account: " + accounts[0]);
      }
    });

    // Carga de los datos del contrato
    App.contracts.Challenge.deployed().then(function(instance) {
      challengeInstance = instance;
      return challengeInstance.challengersCount();
    }).then(function(challengersCount) {
      var challengersResults = $("#challengersResults");
      challengersResults.empty();

      var challengersSelect = $('#challengersSelect');
      challengersSelect.empty();

      for (var i = 1; i <= challengersCount; i++) {
        challengeInstance.challengers(i).then(function(challenger) {
          var id = challenger[0];
          var name = challenger[1];
          var voteCount = challenger[2];

          // Escritura de los datos de los aspirantes
          var challengerTemplate = "<tr><th>" + id  + "</th><td>" + name + "</td><td>" + voteCount + "</td></tr>";
          challengersResults.append(challengerTemplate);

          // Escritura de los nombres de los candidatos en el desplegable
          var challengerOption = "<option value='" + id + "' >" + name + "</ option>";
          challengersSelect.append(challengerOption);
        });
      }
      return challengeInstance.voters(App.account);
  }).then(function(hasVoted) {
    // Cerrar la opción de votar cuando ya se haya votado
    if(hasVoted) {
      $('form').hide();
    }
      loader.hide();
      content.show();
    }).catch(function(error) {
      console.warn(error);
    });
  },

  castVote: function() {
    var challengerId = $('#challengersSelect').val();
    App.contracts.Challenge.deployed().then(function(instance) {
      return instance.vote(challengerId, { from: App.account });
    }).then(function(result) {
      // Esperar al voto hasta actualizar
      $("#content").hide();
      $("#loader").show();
    }).catch(function(err) {
      console.error(err);
    });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});